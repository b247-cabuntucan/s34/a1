const express = require('express');
app = express()
port = 4000
users = [{useraname: 'John', password: 'password123'}]

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.listen(port, () => console.log(`Server is running at port ${port}...`))


app.get('/home', (req, res) => {
    res.send('Welcome to the home page!')
});

app.get('/users', (req, res) => {
    res.send(users);
});

app.delete('/delete-user', (req, res) => {
    res.send(`User ${req.body.username} has been deleted.`)
})